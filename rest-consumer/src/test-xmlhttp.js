import { LitElement, html, css } from 'lit-element';

class TestXmlhttp  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: { type:Object }
    };
  }

  constructor() {
    super();
    this.cargarPlaneta();
  }

  render() {
    return html`
      <p>
        <code>${this.planet}</code>
      </p>
    `;
  }

  cargarPlaneta(){ //xmlHttpRequest
      var req =  new XMLHttpRequest();
      req.open('GET', "https://swapi.dev/api/planets/1", true);
      req.onreadystatechange =
          ((aEvt) => {
            if(req.readyState === 4){ //Si la resp es larga se envía en paquetes y el código 4 significa que terminó de procesar los paquetes recibidos.
                if(req.status === 200){ //200 representa el código de respuesta Ok de la petición. 
                    this.planet = req.responseText;
                    this.requestUpdate();
                }else{
                    alert("Error en llamado REST");
                }
            }
          });
        req.send(null);
  }

}

customElements.define('test-xmlhttp', TestXmlhttp);